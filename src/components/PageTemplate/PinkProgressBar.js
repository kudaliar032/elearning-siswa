const PinkProgressBar = props => {
  const percent = props.maxVal === 0 ? 100 : 100 / (props.maxVal/props.valNow);
  return (
    <div className="progress p-0">
      <div className="progress-bar bg-pink rounded-lg"
           role="progressbar" aria-valuenow={props.valNow} aria-valuemin={0} aria-valuemax={props.maxVal}
           style={{width: `${percent}%`}}>{`${percent.toPrecision(3)}%`}</div>
    </div>
  )
};

export default PinkProgressBar;