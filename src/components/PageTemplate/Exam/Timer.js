import {Component} from 'react';
import moment from "moment";

const pad = (n, width, z) => {z = z || '0';n = n + '';return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;};

class Timer extends Component {
  constructor(props) {
    super(props);
    const {startExam} = this.props;

    // count end time
    const startTimeString = moment.unix(startExam).format();
    const endExam = moment(startTimeString).add({hours: 1, minutes: 30, seconds: 0}).unix();

    this.state = {
      startExam, endExam,
      hours: 1,
      minutes: 30,
      seconds: 0,
      timer: null
    }
  }

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  startTimer() {
    this.state.timer = setInterval(() => {
      const timeNow = moment().unix();
      const {endExam} = this.state;
      const durationTime = endExam - timeNow;
      const duration = moment.duration(durationTime * 1000, 'milliseconds');
      if (durationTime > 0) {
        this.setState({
          hours: duration.hours(),
          minutes: duration.minutes(),
          seconds: duration.seconds()
        });
      } else {
        this.stopTimer();
        this.setState({
          hours: 0,
          minutes: 0,
          seconds: 0
        });
        this.props.timeoutFunc();
      }
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.state.timer);
  }

  render() {
    const {hours, minutes, seconds} = this.state;
    return (
      <div className="col text-center timer">
        <div className="d-flex align-items-end justify-content-center">
          <h1>{pad(hours, 2)}</h1><h6 className="px-2">jam</h6>
          <h1>{pad(minutes, 2)}</h1><h6 className="px-2">menit</h6>
          <h1>{pad(seconds, 2)}</h1><h6 className="px-2">detik</h6>
        </div>
        <h5>waktu tersisa</h5>
      </div>
    )
  }
}

export default Timer;