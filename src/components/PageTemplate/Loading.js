import {Component} from 'react';
import {BounceLoader}  from 'react-spinners';

class Loading extends Component {
  render() {
    const {loading} = this.props;
    return (
      <div className={`loading ${loading ? 'active' : null}`}>
        <BounceLoader size={50} color={"#FA607E"}/>
      </div>
    );
  }
}

export default Loading;