import {Component} from "react";
import Link from "next/link";
import Router from "next/router";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faChartBar, faFolder, faHome, faPencilAlt, faQuestionCircle} from "@fortawesome/free-solid-svg-icons";
import Loading from "./Loading";

class LeftSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  async changeMenu(href, e) {
    const {pathname} = Router;
    e.preventDefault();
    if (pathname !== href) {
      await this.setState({loading: true});
      await Router.push(href);
    }
  }

  render() {
    return (
      <>
        <Loading loading={this.state.loading}/>
        {/*Main Sidebar Container*/}
        <aside className="main-sidebar sidebar-light-dark">
          {/*Brand Logo*/}
          <div className="d-flex justify-content-center pt-4 pb-0">
            <a href="/"
               onClick={e => {
                 this.changeMenu('/', e)
               }}
               className="brand-link border-bottom-0 m-0" style={{width: "auto"}}>
              <img src="/img/logo.svg"
                   alt="E-Learning"
                   className="brand-image mr-3"
                   height={200}
                   width={200}
              />
              <span className="brand-text font-weight-bold">E-Learning</span>
            </a>
          </div>

          {/*Sidebar*/}
          <div className="sidebar">

            {/*Sidebar Menu*/}
            <nav className="h-100">
              <ul className="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu"
                  data-accordion="false">
                <li className="nav-item"></li>
                <li className="nav-header">Menu Utama</li>
                <li className="nav-item">
                  <a href="/"
                     onClick={e => {
                       this.changeMenu('/', e)
                     }}
                     className={Router.pathname === '/' ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faHome} className="nav-icon"/>
                    <p>Beranda</p>
                  </a>
                </li>
                <li className="nav-item">
                  <a href="/course"
                     onClick={e => {
                       this.changeMenu('/course', e)
                     }}
                     className={Router.pathname.includes('/course') && Router.pathname !== '/course/all' ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faBook} className="nav-icon"/>
                    <p>Materiku</p>
                  </a>
                </li>
                <li className="nav-item">
                  <a href="/course/all"
                     onClick={e => {
                       this.changeMenu('/course/all', e)
                     }}
                     className={Router.pathname.includes('/course/all') ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faFolder} className="nav-icon"/>
                    <p>Materi Lain</p>
                  </a>
                </li>
                <li className="nav-item">
                  <a href="/result"
                     onClick={e => {
                       this.changeMenu('/result', e)
                     }}
                     className={Router.pathname.includes('/result') ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faChartBar} className="nav-icon"/>
                    <p>Hasil Belajar</p>
                  </a>
                </li>
                <li className="nav-header">Akun Saya</li>
                <li className="nav-item">
                  <a href="/profile"
                     onClick={e => {
                       this.changeMenu('/profile', e)
                     }}
                     className={Router.pathname.includes('/profile') ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faPencilAlt} className="nav-icon"/>
                    <p>Edit Profile</p>
                  </a>
                </li>
                <li className="nav-item">
                  <a href="/faq"
                     onClick={e => {
                       this.changeMenu('/faq', e)
                     }}
                     className={Router.pathname.includes('/faq') ? 'nav-link active' : 'nav-link'}>
                    <FontAwesomeIcon icon={faQuestionCircle} className="nav-icon"/>
                    <p>Pusat Bantuan</p>
                  </a>
                </li>
              </ul>
            </nav>
            {/*/.sidebar-menu*/}
          </div>
          {/*/.sidebar*/}
        </aside>
      </>
    )
  }
}

export default LeftSidebar;