import {Component} from 'react';
import PinkProgressBar from "../PinkProgressBar";
import Loading from "../Loading";
import Router from 'next/router';

class CardMyCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  async openCourse() {
    await this.setState({loading: true});
    Router.push('/course/[courseId]', `/course/${this.props.id}`);
  }

  render() {
    return (
      <div className="card shadow-none my-course-card" onClick={this.openCourse.bind(this)}>
        <Loading loading={this.state.loading}/>
        <img className="card-img-top" src={`${process.env.NEXT_PUBLIC_FILE}/header_photos/${this.props.header}`}
             alt={this.props.name}/>
        <div className="card-body p-2 px-3">
          <h5 className="card-title">{this.props.courseName}</h5>
          <p className="card-text m-0 mb-2"><small className="text-muted">{this.props.courseStudy}</small></p>
          <PinkProgressBar valNow={this.props.doneModules} maxVal={this.props.totalModules}/>
          <p className="card-text mt-1"><small
            className="text-muted total-module">{`${this.props.doneModules} dari ${this.props.totalModules} Module telah selesai`}</small>
          </p>
        </div>
      </div>
    );
  }
}

export default CardMyCourse;