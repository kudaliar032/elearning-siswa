import {Component} from 'react';
import axios from 'axios';
import {handleAuthenticatedError} from "../../../../utils/auth";
import Swal from 'sweetalert2';
import Router from 'next/router';
import Loading from "../Loading";

class CardAllCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  async takeCourse(id) {
    try {
      const {courseName} = this.props;
      await this.setState({loading: true});
      await axios.post(`/user/take/course/${id}`);
      await this.setState({loading: false});
      Swal.fire({
        icon: "success",
        title: "Sukses",
        text: `Selamat, ${courseName} telah berhasil diambil`,
        showCancelButton: true,
        cancelButtonText: 'Tutup',
        confirmButtonText: 'Buka Materi'
      }).then(({value}) => {
        if (value) {
          Router.push('/course/[courseId]', `/course/${id}`);
        } else {
          this.props.updateAllData();
        }
      });
    } catch (e) {
      handleAuthenticatedError(e, null);
    }
  }

  render() {
    const {id, header, courseName, courseStudy, totalModules} = this.props;
    return (
      <div className="col-4 all-course-card">
        <Loading loading={this.state.loading}/>
        <div className="card shadow-none">
          <img className="card-img-top" src={`${process.env.NEXT_PUBLIC_FILE}/header_photos/${header}`} alt={courseName}/>
          <div className="card-body p-2 px-3 d-flex flex-column">
            <h5 className="card-title">{courseName}</h5>
            <div className="row align-items-center pt-1">
              <div className="col-8">
                <p className="card-text m-0"><small className="text-muted">{courseStudy}</small></p>
                <p className="card-text m-0"><small className="text-muted total-module">{totalModules} Modul
                  tersedia</small></p>
              </div>
              <div className="col-4">
                <button onClick={() => {
                  this.takeCourse(id)
                }} className="btn btn-block p-2 border-0">Ambil
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CardAllCourse;