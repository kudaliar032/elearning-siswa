import {Component} from 'react';
import moment from "moment";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faBookOpen, faIdCard, faSquare, faTrophy} from "@fortawesome/free-solid-svg-icons";

moment.locale('id');

class CardHistoryActivity extends Component {
  render() {
    const {date, description, type} = this.props;
    return (
      <div className="d-flex my-3">
        <div>
          <span className="fa-layers fa-fw fa-4x">
            <FontAwesomeIcon icon={faSquare} color="#7DBCFF"/>
            {type === 'update_detail' ? <FontAwesomeIcon icon={faIdCard} inverse transform="shrink-10"/> : null }
            {type === 'open_module' ? <FontAwesomeIcon icon={faBookOpen} inverse transform="shrink-10"/> : null }
            {type === 'take_course' ? <FontAwesomeIcon icon={faBook} inverse transform="shrink-10"/> : null }
            {type === 'take_exam' ? <FontAwesomeIcon icon={faTrophy} inverse transform="shrink-10"/> : null }
          </span>
        </div>
        <div className="card border-0 shadow-none">
          <div className="card-body border-0 p-0 pl-2">
            <div className="tanggal-riwayat">{moment.unix(date).format('LLLL')}</div>
            <div className="detail-riwayat">{description}</div>
          </div>
        </div>
      </div>
    )
  }
}

export default CardHistoryActivity;