import {Component} from 'react';
import NavBar from "./NavBar";
import LeftSidebar from "./LeftSidebar";
import '../../../public/assets/adminlte/dist/js/adminlte';
import Head from "next/head";
import Loading from "./Loading";

class PageTemplate extends Component {
  render() {
    return (
      <div className="poppins-font h-100 my-bg">
        <Head>
          <title>E-Learning | {this.props.title}</title>
        </Head>
        <NavBar searchBar={this.props.searchBar} searchFunction={this.props.searchFunction}/>
        <LeftSidebar/>
        <div className="main-content content-wrapper pt-4 pb-5">
          {/*Main content*/}
          <section className="content">
            <div className="container">
              {this.props.children}
            </div>
          </section>
        </div>
        <Loading loading={this.props.loading}/>
      </div>
    )
  }
}

export default PageTemplate;