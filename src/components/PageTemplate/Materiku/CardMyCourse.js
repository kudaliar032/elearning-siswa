import {Component} from 'react';
import PinkProgressBar from "../PinkProgressBar";
import Link from "next/link";

class CardMyCourse extends Component {
  render() {
    const {id, study, name, totalOpenedModules, totalModules, header} = this.props;
    const icon = {
      'IPA': 'icon-ipa.svg',
      'Bahasa Indonesia': 'icon-bahasa-indonesia.svg',
      'Bahasa Inggris': 'icon-bahasa-inggris.svg',
      'Matematika': 'icon-matematika.svg'
    };
    return (
      <Link href="/course/[courseId]" as={`/course/${id}`}>
        <div className="col-6 col-md-4 rounded-lg my-course-card">
          <div className="card course-card shadow-none">
            <div className="card-header border-0">
              <div className="row align-items-center">
                <div className="border-bottom">
                  <img src={`/img/${icon[study]}`} className="img-fluid course-study-icon"/>
                </div>
                <div className="pl-3 mr-auto">
                  <h6 className="course-study mr-auto my-0">{study}</h6>
                </div>
                <div className="p-0 col-4">
                  {
                    totalOpenedModules < totalModules ? (
                      <p className="course-status-blue py-1 m-0 rounded-lg">Sedang Berlangsung</p>
                    ) : (
                      <p className="course-status-pink py-1 m-0 rounded-lg">Selesai</p>
                    )
                  }
                </div>
              </div>
            </div>
            <div className="card-body no-border px-3 py-0">
              <p className="card-title">{name}</p>
              <p className="card-text pt-2">Stoikiometri adalah ilmu kimia yang mempelajari tentang kuantitas
                suatu
                zat, meliputi massa, jumlah mol, volume, dan jumlah partikel.</p>
              <PinkProgressBar valNow={totalOpenedModules} maxVal={totalModules}/>
              <p className="mt-2"><small className="text-muted total-module">{totalOpenedModules} dari {totalModules} Module telah
                selesai</small></p>
            </div>
          </div>
        </div>
      </Link>
    )
  }
}

export default CardMyCourse;