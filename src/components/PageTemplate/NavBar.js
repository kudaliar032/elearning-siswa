import {Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars, faSearch, faSignOutAlt, faTimesCircle} from "@fortawesome/free-solid-svg-icons";
import {connect} from 'react-redux';
import Router from "next/router";
import Swal from 'sweetalert2';
import Loading from "./Loading";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchValue: ''
    }
  }

  componentDidMount() {
    const searchValue = Router.query.name;
    searchValue ? this.setState({searchValue}) : null;
  }

  searchChange(e) {
    e.preventDefault();
    this.setState({searchValue: e.target.value});
  }

  searchSubmit(e) {
    e.preventDefault();
    this.props.searchFunction(this.state.searchValue);
  }

  logout() {
    Swal.fire({
      icon: 'question',
      title: 'Keluar?',
      html: 'Anda yakin keluar dari E-Learning',
      cancelButtonText: 'Batal',
      confirmButtonText: 'Keluar',
      showCancelButton: true,
      heightAuto: false
    }).then(({value}) => {
      if(value) {
        Router.push('/user/logout');
      }
    })
  }

  async openProfile() {
    await this.setState({loading: true});
    Router.push('/profile');
  }

  render() {
    const {photo} = this.props.userProfile.user;
    const {pathname} = Router;
    return (
      <nav className="main-header navbar navbar-expand layout-navbar-fixed navbar-light border-bottom-0 pb-3 pt-4">
        <Loading loading={this.state.loading}/>
        <div className="container">
          {/*Left navbar links*/}
          <ul className="navbar-nav">
            <li className="nav-item">
              <button className="btn nav-link" data-widget="pushmenu"><FontAwesomeIcon icon={faBars}/></button>
            </li>
          </ul>

          {/*SEARCH FORM*/}
          {
            this.props.searchBar ? (
              <form className="form-inline my-search ml-3 mr-auto rounded-lg" onSubmit={this.searchSubmit.bind(this)}>
                <div className="input-group">
                  <input
                    className="form-control form-control-navbar"
                    type="text"
                    placeholder="Pencarian"
                    aria-label="Search" size={65}
                    value={this.state.searchValue}
                    onChange={this.searchChange.bind(this)}
                  />
                  <div className="input-group-append">
                    {
                      this.state.searchValue ? (
                        <button type="button" className="btn clean-button px-0 pr-1" onClick={async () => {
                          await this.setState({searchValue: ""});
                          if (pathname !== "/") {
                            document.getElementById('searchSubmit').click();
                          }
                        }}>
                          <FontAwesomeIcon icon={faTimesCircle}/>
                        </button>
                      ) : null
                    }
                    <button type="submit" id="searchSubmit" className="btn btn-outline-secondary">
                      <FontAwesomeIcon icon={faSearch}/>
                    </button>
                  </div>
                </div>
              </form>
            ) : null
          }

          {/*Right navbar links*/}
          <ul className="navbar-nav">
            <li className="nav-item" style={{
              display: ['/profile', '/result'].includes(Router.pathname) ? 'none' : 'block'
            }}>
              <button className="btn nav-link" onClick={this.openProfile.bind(this)}>
                <img
                  className="photo-profile rounded-circle"
                  src={`${process.env.NEXT_PUBLIC_API}/user/photo/${photo ? photo : 'default.png'}`}
                  alt={this.props.userProfile.user.name}/>
              </button>
            </li>
            <li className="nav-item">
              <button className="btn nav-link" onClick={this.logout.bind(this)}>
                <FontAwesomeIcon icon={faSignOutAlt} size="2x"/>
              </button>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

const mapStateToProps = state => ({
  userProfile: state.userProfile
});

export default connect(mapStateToProps)(NavBar);