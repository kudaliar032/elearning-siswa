import {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faMinus, faPlus} from "@fortawesome/free-solid-svg-icons";

class FaqList extends Component {
  render() {
    const {id, question, answer} = this.props;
    return (
      <div className="card border-0 shadow mb-4">
        <div className="card-header border-0" id={`heading${id}`}>
          <div className="d-flex align-items-center">
            <h6 className="mb-0 py-3 selection faq-item mr-auto collapsed" data-toggle="collapse" data-target={`#collapse${id}`}
                aria-expanded="true"
                aria-controls={`collapse${id}`}>
              {question}
            </h6>
            <FontAwesomeIcon icon={faPlus} size="lg" className="mx-2 icon-plus"/>
            <FontAwesomeIcon icon={faMinus} size="lg" className="mx-2 icon-minus"/>
          </div>
        </div>
        <div id={`collapse${id}`} className="collapse" aria-labelledby={`heading${id}`} data-parent="#accordion">
          <div className="card-body border-0 pt-0">{answer}</div>
        </div>
      </div>
    );
  }
}

export default FaqList;