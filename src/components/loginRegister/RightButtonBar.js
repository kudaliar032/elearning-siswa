import React, {Component} from 'react';
import Link from 'next/link';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";

export default class RightButtonBar extends Component {
  render() {
    return (
      <Link href={this.props.href}>
        <div className="row shadow pt-4 pb-3 pr-3 pl-4 rounded-lg mx-3 align-items-center mt-4 bar-style">
          <div className="col-11">
            <h5>{this.props.title}</h5>
            <p className="text-blue-white">{this.props.subtitle}</p>
          </div>
          <div className="col-1">
            <FontAwesomeIcon icon={faArrowRight} size="lg" className="lr-arrow"/>
          </div>
        </div>
      </Link>
    );
  }
}