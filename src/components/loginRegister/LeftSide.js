import React, {Component} from 'react';

export default class LeftSide extends Component {
  render() {
    return (
      <div className="d-flex flex-column justify-content-center h-100 mx-4 px-4">
        <h1 className="py-3">Selamat Datang di Aplikasi E-Learning</h1>
        <p className="py-1 text-gray-blue">Aplikasi ini akan membantumu mencapai tujuan belajarmu!</p>
        <p  className="py-1 text-gray-blue">Solusi terlengkap dalam satu aplikasi yang telah dipercaya untuk belajar secara online. Belajar bersama aplikasi E-learning kami akan membantumu untuk tumbuh menjadi lebih maju. </p>
      </div>
    );
  }
}