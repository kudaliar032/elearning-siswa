import {CREATE_MULTIPLECHOICES, RESET_MULTIPLECHOICES, UPDATE_ANSWER} from "../actions/multipleChoices";

const initialState = [];

export function multipleChoices(state = initialState, action) {
  switch (action.type) {
    case CREATE_MULTIPLECHOICES:
      return [
        ...state,
        ...action.data
      ];
    case UPDATE_ANSWER:
      return state.map(item => {
        if (item.id === action.id) {
          return {
            ...item,
            answer: action.answer
          }
        }
        return item
      });
    case RESET_MULTIPLECHOICES:
      return initialState;
    default:
      return state;
  }
}