import {SAVE_PROFILE, RESET_PROFILE} from "../actions/userProfile";

const initialState = {};

export function userProfile(state = initialState, action) {
  switch (action.type) {
    case SAVE_PROFILE:
      return Object.assign({}, state, {
        user: action.data.user,
        detail: action.data.detail
      });
    case RESET_PROFILE:
      return initialState;
    default:
      return state;
  }
}