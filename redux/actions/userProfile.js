export const SAVE_PROFILE = 'SAVE_PROFILE';
export const RESET_PROFILE = 'RESET_PROFILE';

import axios from 'axios';
import {handleAuthenticatedError} from "../../utils/auth";

export const saveProfile = data => ({
  type: SAVE_PROFILE,
  data
});

export const resetProfile = () => ({
  type: RESET_PROFILE
});

export const getProfile = token => (dispatch => (
  axios.get('/user/detail', {headers: {Authorization: `Bearer ${token}`}}).then(res => {
    dispatch(saveProfile(res.data));
  }).catch(err => {
    handleAuthenticatedError(err, null);
  })
));