import axios from 'axios';
import {handleAuthenticatedError} from "../../utils/auth";
import Router from "next/router";

export const ADD_ANSWER = 'ADD_ANSWER';
export const UPDATE_ANSWER = 'UPDATE_ANSWER';
export const RESET_ANSWERS = 'RESET_ANSWERS';
export const CREATE_MULTIPLECHOICES = 'CREATE_MULTIPLECHOICES';
export const RESET_MULTIPLECHOICES = 'RESET_MULTIPLECHOICES';

export const createMultipleChoices = data => ({type: CREATE_MULTIPLECHOICES, data});
export const resetMultipleChoices = () => ({type: RESET_MULTIPLECHOICES});
export const updateAnswer = (id, answer) => ({type: UPDATE_ANSWER, id, answer});

export const fetchMultipleChoices = courseId => (dispatch => {
  dispatch(resetMultipleChoices()); //reset data
  axios.get(`/exam/questions/${courseId}`).then(res => {
    dispatch(createMultipleChoices(res.data)); //add new fetch data
    Router.push('/course/[courseId]/exams?no=1', `/course/${courseId}/exams?no=1`); //route to exam page
  }).catch(err => {
    handleAuthenticatedError(err, null);
  });
});