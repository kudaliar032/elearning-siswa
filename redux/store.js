import {userProfile} from "./reducers/userProfile";
import {multipleChoices} from "./reducers/multipleChoices";

import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from 'redux-thunk';
import {persistStore} from 'redux-persist';
import logger from 'redux-logger';

const reducers = combineReducers({userProfile, multipleChoices});

const Redux = (initialState) => {
  let store;
  const isClient = typeof window !== 'undefined';
  if (isClient) {
    const {persistReducer} = require('redux-persist');
    const storage = require('redux-persist/lib/storage').default;

    const persistConfig = {
      key: 'root',
      storage,
      whitelist: ['userProfile', 'multipleChoices']
    };

    store = createStore(
      persistReducer(persistConfig, reducers),
      initialState,
      applyMiddleware(thunkMiddleware)
      // applyMiddleware(logger, thunkMiddleware)
    );

    store.__PERSISTOR = persistStore(store);
  } else {
    store = createStore(
      reducers,
      initialState,
      applyMiddleware(thunkMiddleware)
      // applyMiddleware(logger, thunkMiddleware)
    );
  }

  return store;
}

export default Redux;