import {Component} from 'react';
import dynamic from "next/dynamic";
import CardMyCourse from "../src/components/PageTemplate/Materiku/CardMyCourse";
import {handleAuthenticatedError, initialSetAxiosToken} from "../utils/auth";
import axios from 'axios';
import Router from "next/router";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons";

const PageTemplate = dynamic(() => import('../src/components/PageTemplate'), {ssr: false});

class page extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const {study, name} = ctx.query;
      let myCourse;
      if (study && name) {myCourse = await axios.get(`/user/course/list?study=${study}&name=${name}`)}
      else if (study) {myCourse = await axios.get(`/user/course/list?study=${study}`)}
      else if (name) {myCourse = await axios.get(`/user/course/list?name=${name}`)}
      else {myCourse = await axios.get('/user/course/list')}

      return {
        myCourse: myCourse.data
      }
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {myCourse} = this.props;
    const {study, name} = Router.query;
    this.state = {
      myCourse: myCourse ? myCourse : [],
      study, name
    };

    this.updateCourseList = this.updateCourseList.bind(this);
  }

  updateCourseList(study, name) {
    let axiosUrl, routeUrl;
    if (study && name) {
      axiosUrl = `/user/course/list?study=${study}&name=${name}`;
      routeUrl = `/course?study=${study}&name=${name}`;
    }
    else if (study) {
      axiosUrl = `/user/course/list?study=${study}`;
      routeUrl = `/course?study=${study}`;
    }
    else if (name) {
      axiosUrl = `/user/course/list?name=${name}`;
      routeUrl = `/course?name=${name}`;
    }
    else {
      axiosUrl = `/user/course/list`;
      routeUrl = `/course`;
    }

    axios.get(axiosUrl).then(async res => {
      this.setState({myCourse: res.data, study, name});
      Router.push(routeUrl);
    }).catch(err => {handleAuthenticatedError(err, null);});
  };

  searchFunction(searchValue) {
    const {study} = this.state;
    // console.log(study, searchValue);
    this.updateCourseList(study, searchValue);
  }

  render() {
    const {myCourse, study, name} = this.state;
    const studyName = {matematika: 'Matematika', ipa: 'IPA', indonesia: 'Bahasa Indonesia', inggris: 'Bahasa Inggris'};
    return (
      <PageTemplate title="Materiku" searchBar searchFunction={this.searchFunction.bind(this)}>
        <div className="row mb-4">
          <section className="col">
            <div className="d-flex flex-row course-header align-items-center">
              <div className="d-flex mr-auto align-items-center">
                <h5>Materi saya dari <span>{study ? '"'+studyName[study]+'"' : '"Semua Materi"'}</span></h5>
                {name ? (
                  <h5>, dengan pencarian <span>"{name}"</span></h5>
                ) : null}
              </div>
              <div>
                <div className="btn-group filter-by">
                  <button type="button" className="btn border-0">
                    <span>Kategori:</span> {study ? studyName[study] : 'Semua Materi'}
                  </button>
                  {study ?
                    <button onClick={() => {this.updateCourseList(null, name)}} type="button" className="btn border-0 reset">
                      <FontAwesomeIcon icon={faTimesCircle}/>
                    </button> : null}
                  <button type="button" className="btn dropdown-toggle border-0 icon" data-toggle="dropdown" aria-haspopup="true"
                          aria-expanded="false" data-offset="0, 10"></button>
                  <div className="dropdown-menu dropdown-menu-right">
                    <Link href="/course?study=matematika">
                      <a onClick={() => {
                        this.updateCourseList('matematika', name)
                      }} className={`dropdown-item ${study === 'matematika' ? 'active' : null}`}>Matematika</a>
                    </Link>
                    <hr/>
                    <Link href="/course?study=ipa">
                      <a onClick={() => {
                        this.updateCourseList('ipa', name)
                      }} className={`dropdown-item ${study === 'ipa' ? 'active' : null}`}>IPA</a>
                    </Link>
                    <hr/>
                    <Link href="/course?study=indonesia">
                      <a onClick={() => {
                        this.updateCourseList('indonesia', name)
                      }} className={`dropdown-item ${study === 'indonesia' ? 'active' : null}`}>Bahasa Indonesia</a>
                    </Link>
                    <hr/>
                    <Link href="/course?study=inggris">
                      <a onClick={() => {
                        this.updateCourseList('inggris', name)
                      }} className={`dropdown-item ${study === 'inggris' ? 'active' : null}`}>Bahasa Inggris</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="row">
          <section className="col">
            <div className={myCourse.length > 0 ? 'row row-cols-2 row-cols-md-3' : 'row row-col-1 p-2'}>
              {
                myCourse.length > 0 ? myCourse.map((val, key) => (
                  <CardMyCourse
                    key={key}
                    id={val.id}
                    name={val.name}
                    level={val.level}
                    study={val.study}
                    header={val.header}
                    totalOpenedModules={val.total_opened_modules}
                    totalModules={val.total_modules}
                  />
                )) : (
                  <h3 className="text-gray-blue">Tidak ditemukan materi</h3>
                )
              }
            </div>
          </section>
        </div>
      </PageTemplate>
    );
  }
}

export default page;