import dynamic from "next/dynamic";
import {handleAuthenticatedError, initialSetAxiosToken} from "../utils/auth";
import {Component} from 'react';
import CardMyCourse from "../src/components/PageTemplate/Beranda/CardMyCourse";
import CardHistoryActivity from "../src/components/PageTemplate/Beranda/CardHistoryActivity";
import CardAllCourse from "../src/components/PageTemplate/Beranda/CardAllCourse";
import {connect} from "react-redux";
import axios from 'axios';
import Link from "next/link";
import Router from 'next/router';

const PageTemplate = dynamic(() => import('../src/components/PageTemplate'), {ssr: false});

class index extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const allCourse = await axios.get('/course/list/not');
      const myCourses = await axios.get('/user/course/list');
      const myActivity = await axios.get('/user/activity/4');
      const lastModule = await axios.get('/module/last');

      return {
        allCourse: allCourse.data,
        myCourses: myCourses.data,
        myActivity: myActivity.data,
        lastModule: lastModule.data
      };
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {allCourse, myCourses, myActivity, lastModule} = this.props;
    this.state = {
      loading: true,
      allCourse: allCourse ? allCourse : [],
      myCourses: myCourses ? myCourses : [],
      myActivity: myActivity ? myActivity : [],
      lastModule: lastModule ? lastModule : [],
      name: ''
    }
  }

  componentDidMount() {
    this.setState({loading: false});
  }

  async updateAllData() {
    try {
      await this.setState({loading: true});
      const allCourse = await axios.get('/course/list/not');
      const myCourses = await axios.get('/user/course/list');
      const myActivity = await axios.get('/user/activity/4');
      const lastModule = await axios.get('/module/last');
      this.setState({
        allCourse: allCourse.data,
        myCourses: myCourses.data,
        myActivity: myActivity.data,
        lastModule: lastModule.data
      });
      await this.setState({loading: false});
    } catch (e) {
      handleAuthenticatedError(e, null);
    }
  }

  searchFunction(value) {
    Router.push(`/course/all?name=${value}`);
  }

  render() {
    const {name} = this.props.userProfile.user;

    let {allCourse, myCourses, myActivity, lastModule} = this.state;
    allCourse = allCourse.length >= 3 ? allCourse.slice(0, 3) : allCourse;
    myCourses = myCourses.length >= 2 ? myCourses.slice(0, 2) : myCourses;

    // console.log(lastModule);
    return (
      <PageTemplate title="Beranda" searchBar searchFunction={this.searchFunction.bind(this)} loading={this.state.loading}>
        <div className="row">
          <section className="col-lg-8 pr-5">
            {/*Welcome Card*/}
            <div className="row">
              <div className="col">
                <div className="card text-white mb-3 welcome-card p-2">
                  <div className="card-body">
                    <p className="card-title">Selamat Datang <b>{name}</b>!</p>
                    {
                      lastModule.course_id ? lastModule.total_open ? (
                        <p className="card-text">
                          Kamu sedang mengerjakan <Link href="/course/[courseId]" as={`/course/${lastModule.course_id}`}>
                          <a>{lastModule.course_name}</a></Link>, dengan persentase module selesai <span>
                          {((lastModule.total_open / lastModule.total_module) * 100).toPrecision(3)}%</span>. Yuk lanjutkan belajarmu!
                        </p>
                      ) : (
                        <p className="card-text">
                          Kamu sedang mengerjakan <Link href="/course/[courseId]" as={`/course/${lastModule.course_id}`}>
                          <a>{lastModule.course_name}</a></Link>, Yuk lanjutkan belajarmu!
                        </p>
                      ) : (
                        <p className="card-text">
                          Kamu belum mengerjakan materi apapun, silahkan ambil <Link
                          href="/course/all"><a>Materi</a></Link> untuk
                          memulai belajar.
                        </p>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>

            {/*My Course*/}
            {
              myCourses.length > 0 ? (
                <div className="row mt-2">
                  <div className="col">
                    <div className="card bg-transparent border-0 shadow-none">
                      <div className="card-header border-0 p-0">
                        <h3 className="card-title">Materi yang diambil</h3>
                        <div className="card-tools pr-3">
                          <Link href='/course'><a><p className="text-pink font-weight-bolder">Lihat Semua</p></a></Link>
                        </div>
                      </div>
                      <div className="row row-cols-2">
                        {
                          myCourses.map((val, key) => (
                            <div className="col-6" key={key}>
                              <CardMyCourse
                                id={val.id}
                                courseName={val.name}
                                courseStudy={val.study}
                                header={val.header}
                                totalModules={val.total_modules}
                                doneModules={val.total_opened_modules}/>
                            </div>
                          ))
                        }
                      </div>
                    </div>
                  </div>
                </div>
              ) : null
            }

          </section>
          <section className="col-lg-4 py-0 pb-3">
            <div className="card my-card shadow-none border-0 bg-transparent h-100">
              <div className="card-header px-0 pt-0 pb-3 border-0">
                <h3 className="card-title">Riwayat Belajar</h3>
              </div>
              <div className="card-body bg-white rounded-lg shadow-lg px-4 py-0 h-100">
                {
                  myActivity.length > 0 ? myActivity.map((val, key) => (
                    <CardHistoryActivity
                      key={key}
                      date={val.date}
                      type={val.type}
                      description={val.description}
                    />
                  )) : (
                    <h5 className="text-center pt-4 text-gray-blue">Belum ada aktivitas</h5>
                  )
                }
              </div>
            </div>
          </section>
        </div>
        {
          allCourse.length > 0 ? (
            <div className="row mt-4">
              <section className="col-lg-12">
                {/*All Course*/}
                <div className="row mt-2">
                  <div className="col">
                    <div className="card bg-transparent border-0 shadow-none">
                      <div className="card-header border-0 p-0">
                        <h3 className="card-title">Materi yang belum diambil</h3>
                        <div className="card-tools pr-3">
                          <Link href="/course/all"><a><p className="text-pink font-weight-bolder">Lihat Semua</p>
                          </a></Link>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="row row-cols-3">
                          {
                            allCourse.map((val, key) => (
                              <CardAllCourse
                                key={key}
                                id={val.id}
                                courseName={val.name}
                                courseStudy={val.study}
                                totalModules={val.total_modules}
                                header={val.header}
                                updateAllData={this.updateAllData.bind(this)}/>
                            ))
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          ) : null
        }
      </PageTemplate>
    )
  }
}

const mapStateToProps = state => ({
  userProfile: state.userProfile
});

export default connect(mapStateToProps)(index);