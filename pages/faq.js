import {Component} from 'react';
import dynamic from "next/dynamic";
import {handleAuthenticatedError, initialSetAxiosToken} from "../utils/auth";
import axios from "axios";
import FaqList from "../src/components/Faq/FaqList";

const PageTemplate = dynamic(() => import('../src/components/PageTemplate'), {ssr: false});

class faq extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const {data} = await axios.get("/faq/list");
      return {faqs: data || []}
    } catch (err) {
      handleAuthenticatedError(err, ctx);
    }
  };

  render() {
    const {faqs} = this.props;
    return (
      <PageTemplate title="Pusat Bantuan">
        <div className="row bg-white rounded-lg shadow-sm justify-content-center faq">
          <div className="col-9 py-5">
            <div className="text-center">
              <h1>Pusat Bantuan</h1>
              <h6 className="subtitle mb-5">Butuh Jawaban? Cari jawabannya di sini!</h6>
              <h6 className="description">Hi! Jika kamu memiliki pertanyaan atau mengalami kesulitan saat mengakses
                halaman pada aplikasi ini
                silahkan cari jawaban di bawah ini.</h6>
            </div>
            <div id="accordion" className="mt-4">
              {
                faqs.map(val => (
                  <FaqList {...val} key={val.id}/>
                ))
              }
            </div>
          </div>
        </div>
      </PageTemplate>
    );
  }
}

export default faq;