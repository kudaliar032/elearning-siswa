import {Component} from 'react';
import dynamic from 'next/dynamic';
import {connect} from 'react-redux';
import {Doughnut, Line} from 'react-chartjs-2';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faCheckCircle} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import CardMyCourse from "../src/components/PageTemplate/Beranda/CardMyCourse";
import axios from 'axios';
import {handleAuthenticatedError, initialSetAxiosToken} from "../utils/auth";

const PageTemplate = dynamic(() => import('../src/components/PageTemplate'), {ssr: false});

const lineGraphOptions = {
  legend: {
    display: false
  },
  layout: {
    padding: {
      top: 25,
      bottom: 25,
    }
  },
  scales: {
    yAxes: [{
      ticks: {
        fontColor: '#A6B4C2',
        suggestedMax: 100,
        suggestedMin: 0,
        stepSize: 10,
        padding: 15
      }
    }],
    xAxes: [{
      ticks: {
        fontColor: '1C2588',
        padding: 10
      }
    }]
  },
  elements: {
    point: {
      backgroundColor: '#1C2588',
      borderWidth: 5,
      borderColor: '#1C2588',
    },
    line: {
      fill: false,
      borderColor: '#1C2588',
      tension: 0
    }
  }
}
const doughnutGraphOptions = {
  cutoutPercentage: 75,
  tooltips: false
}

class result extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const myCourse = await axios.get('/user/course/list');
      const myResult = await axios.get('/user/result');

      return {
        myCourse: myCourse ? myCourse.data : [],
        myResult: myResult.data
      }
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  }

  render() {
    const {myCourse, myResult} = this.props;
    const {
      total_courses: totalCourse,
      total_done_course: totalDoneCourse,
      all_avg_score: allAverage,
      avg_score_month: averageMonth
    } = this.props.myResult;
    const {photo, name} = this.props.userProfile.user;
    return (
      <PageTemplate title="Hasil Belajar">
        <div className="row h-100 result">
          <div className="col-4 pr-3">
            <div className="card shadow h-100 left-panel">
              <div className="card-body">
                <div className="d-flex justify-content-center px-5 pt-4">
                  <img
                    src={`${process.env.NEXT_PUBLIC_API}/user/photo/${photo !== null ? photo : 'default.png'}`}
                    alt={name}
                    className="rounded-circle img-fluid w-75 shadow"
                  />
                </div>
                <h4 className="text-center my-2 mt-4">{name}</h4>
                <div className="text-center mt-4">
                  <p className="course-title"><FontAwesomeIcon icon={faBook}/> Materi yang diambil</p>
                  <h6 className="course-value">{totalCourse} Materi</h6>
                  <p className="course-title"><FontAwesomeIcon icon={faCheckCircle}/> Materi yang telah diselesaikan</p>
                  <h6 className="course-value">{totalDoneCourse} Materi</h6>
                </div>
                <Doughnut
                  data={{
                    datasets: [{
                      backgroundColor: ["#1C2588", "#E3E9EF"],
                      data: [allAverage, 100-allAverage],
                    }]
                  }}
                  options={doughnutGraphOptions}
                />
                <h3 className="text-center my-3 total-score">{allAverage.toFixed(1)}</h3>
                <p className="text-center mx-5 text-score">Nilai rata-rata dari semua latihan soal yang telah dikerjakan</p>
              </div>
            </div>
          </div>
          <div className="col-8 p-0">
            <div className="row row-cols-1">
              <div className="col-12">
                <div className="card shadow rounded-lg welcome">
                  <div className="card-body">
                    <h4 className="py-2">Hasil Belajar {name}</h4>
                    <p>Rata - rata nilai yang kamu miliki adalah <b>{allAverage.toFixed(1)}/100</b>, kamu unggul di mata pelajaran <b>Bahasa Inggris</b>. Yuk
                      tingkatkan lagi semangatmu agar mencapai nilai yang sempurna!</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row row-cols-1">
              <div className="col-12">
                <div className="card shadow rounded-lg h-100 grafik">
                  <div className="card-header pt-4 pb-3">
                    <h5>Grafik Hasil Belajar Bulanan</h5>
                  </div>
                  <div className="card-body h-100">
                    <div className="h-100">
                      <Line
                        height={200}
                        data={{
                          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
                          datasets: [{data: averageMonth}]
                        }}
                        options={lineGraphOptions}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {
          myCourse.length > 0 ? (
            <div className="row mt-5">
              <div className="col">
                <div className="card bg-transparent border-0 shadow-none">
                  <div className="card-header border-0 p-0">
                    <h3 className="card-title">Materi yang diambil</h3>
                    <div className="card-tools pr-3">
                      <Link href='/course'><a><p className="text-pink font-weight-bolder">Lihat Semua</p></a></Link>
                    </div>
                  </div>
                  <div className="row row-cols-3 p-0">
                    {
                      myCourse.map((val, key) => (
                        <div className="col-4" key={key}>
                          <CardMyCourse
                            id={val.id}
                            courseName={val.name}
                            courseStudy={val.study}
                            header={val.header}
                            totalModules={val.total_modules}
                            doneModules={val.total_opened_modules}/>
                        </div>
                      ))
                    }
                  </div>
                </div>
              </div>
            </div>
          ) : null
        }
      </PageTemplate>
    )
  }
}

const mapStateToProps = state => ({
  userProfile: state.userProfile
});

export default connect(mapStateToProps)(result);