import React, {Component} from 'react';
import dynamic from "next/dynamic";
import {connect} from 'react-redux';
import axios from 'axios';
import {handleAuthenticatedError, initialSetAxiosToken} from "../utils/auth";
import {getProfile} from "../redux/actions/userProfile";
import nookies from 'nookies';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons";
import Cropper from 'react-cropper';

const PageTemplate = dynamic(() => import('../src/components/PageTemplate'), {ssr: false});

class profile extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      await axios.post('/login/verify');
      return {_token: nookies.get(ctx)._token}
    } catch (err) {
      handleAuthenticatedError(err, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {id, username, name, photo, email} = this.props.userProfile.user;
    const {detail} = this.props.userProfile;
    this.state = {
      id, username, photo, email,
      firstName: name.split(" ", 1),
      lastName: name.split(" ").slice(1, name.length).join(" "),
      school: detail === null ? '' : detail.sekolah,
      birthPlace: detail === null ? '' : detail.tempat_lahir,
      birthDate: detail === null ? '' : detail.tanggal_lahir,
      photoProfile: `${process.env.NEXT_PUBLIC_API}/user/photo/${photo === null ? 'default.png' : photo}`,
      cropper: null,
      photoChanged: false
    };

    this.cropper = React.createRef();
  }

  changeValue(e) {
    const {value, id} = e.target;
    this.setState({[id]: value})
  }

  saveChange(e) {
    e.preventDefault();
    const {id, email, firstName, lastName, school, birthPlace, birthDate} = this.state;
    const data = {
      id: id,
      email: email,
      name: firstName + " " + lastName,
      sekolah: school,
      tempat_lahir: birthPlace,
      tanggal_lahir: birthDate
    };
    axios.post('/user/detail/update', data).then(res => {
      this.props.getProfile(this.props._token);
    }).catch(err => {
      handleAuthenticatedError(err, null);
    });
  }

  changePhotoProfile(e) {
    this.setState({
      photoProfile: URL.createObjectURL(e.target.files[0]),
      photoChanged: true
    });
  }

  savePhotoProfile() {
    const myCrop = this.cropper.current;
    myCrop.getCroppedCanvas({
      width: 300,
      height: 300
    }).toBlob(blob => {
      const formData = new FormData();
      formData.append('photoProfile', blob);
      axios.post('/user/profile/update', formData).then(async res => {
        await this.setState({photo: null});
        await this.setState({photo: this.state.username+'.png'});
        this.setState({photoChanged: false});
        // console.log(res);
      }).catch(err => {
        handleAuthenticatedError(err, null);
      });
    });
  }

  render() {
    const {username, photo, email, firstName, lastName, school, birthPlace, birthDate, photoProfile, photoChanged} = this.state;
    return (
      <PageTemplate title="Edit Profile">
        <div className="row bg-white rounded-lg edit-profile shadow-sm">
          <section className="offset-2 col-8">
            <div className="row">
              <div className="col d-flex flex-column py-4 px-5 pt-5">
                <div className="card photo-profile mx-auto shadow-none">
                  <img
                    src={`${process.env.NEXT_PUBLIC_API}/user/photo/${photo !== null ? photo : 'default.png'}`}
                    alt={this.props.userProfile.user.name}
                    className="photo-profile rounded-circle"
                  />
                  <div className="card-img-overlay text-black p-2">
                    <button className="btn btn-danger btn-xs rounded-circle border-0" data-toggle="modal"
                            data-target="#editPhotoProfile">
                      <FontAwesomeIcon icon={faPencilAlt}/>
                    </button>
                  </div>

                  <div className="modal fade" id="editPhotoProfile" tabIndex="-1" role="dialog"
                       aria-labelledby="editPhotoProfileLabel" aria-hidden="true" data-backdrop="static">
                    <div className="modal-dialog" role="document">
                      <div className="modal-content">
                        <div className="modal-body">
                          <div className="row px-2 d-flex flex-column">
                            <input type="file" id="changePhotoProfile" onChange={this.changePhotoProfile.bind(this)}/>
                            {
                              photoChanged ? (
                                <Cropper
                                  ref={this.cropper}
                                  src={photoProfile}
                                  aspectRatio={1}
                                  className="img-fluid col mt-3 p-0"
                                  style={{width: '100%', height: '25rem'}}
                                  viewMode={2}
                                  autoCropArea={1}
                                />
                              ) : null
                            }
                          </div>
                        </div>
                        <div className="modal-footer">
                          <button type="button" className="btn btn-danger" data-dismiss="modal">Batal</button>
                          <button type="button" className="btn btn-primary" data-dismiss="modal"
                                  onClick={this.savePhotoProfile.bind(this)}>Simpan
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <h4 className="text-center mt-2 my-0">{this.props.userProfile.user.name}</h4>
                <p className="text-center">{this.props.userProfile.user.email}</p>
                <form className="profile-form pb-5 px-5" onSubmit={this.saveChange.bind(this)}>
                  <div className="form-group">
                    <label>Username</label>
                    <input type="text" id="username" className="form-control" value={username} disabled/>
                  </div>
                  <div className="form-group">
                    <label>Email</label>
                    <input type="email" id="email" className="form-control" value={email}
                           onChange={this.changeValue.bind(this)}/>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <label>Nama Depan</label>
                      <input type="text" id="firstName" className="form-control" value={firstName}
                             onChange={this.changeValue.bind(this)}/>
                    </div>
                    <div className="form-group col-md-6">
                      <label>Nama Belakang</label>
                      <input type="text" id="lastName" className="form-control" value={lastName}
                             onChange={this.changeValue.bind(this)}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Nama Sekolah</label>
                    <input type="text" id="school" className="form-control" value={school}
                           onChange={this.changeValue.bind(this)}/>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <label>Tempat Lahir</label>
                      <input type="text" id="birthPlace" className="form-control" value={birthPlace}
                             onChange={this.changeValue.bind(this)}/>
                    </div>
                    <div className="form-group col-md-6">
                      <label>Tanggal Lahir</label>
                      <input type="text" id="birthDate" className="form-control" value={birthDate}
                             onChange={this.changeValue.bind(this)}/>
                    </div>
                  </div>

                  <button type="submit" className="btn btn-block py-2">Simpan Perubahan</button>
                </form>
              </div>
            </div>
          </section>
        </div>
      </PageTemplate>
    )
  }
}

const mapStateToProps = state => ({
  userProfile: state.userProfile
});

const mapDispatchToProps = dispatch => ({
  getProfile: _token => dispatch(getProfile(_token))
});

export default connect(mapStateToProps, mapDispatchToProps)(profile);