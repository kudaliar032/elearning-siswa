import Document, {Html, Head, Main, NextScript} from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return {...initialProps}
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="stylesheet" href="/assets/ionicons/css/ionicons.min.css"/>
          <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css"/>
          <link rel="stylesheet" href="/assets/adminlte/dist/css/adminlte.min.css"/>
          <link rel="stylesheet" href="/assets/overlayScrollbars/css/OverlayScrollbars.min.css"/>
          <link rel="icon" href="/favicon.svg"/>
        </Head>
        <body className="hold-transition sidebar-mini layout-fixed h-100">
        <Main/>
        <NextScript/>
        <script src="/assets/js/jquery-3.4.1.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.bundle.js"></script>
        <script src="/assets/overlayScrollbars/js/OverlayScrollbars.min.js"></script>
        </body>
      </Html>
    )
  }
}

export default MyDocument