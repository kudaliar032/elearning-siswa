import App from "next/app";
import {Provider} from 'react-redux';
import reduxStore from "../redux/store";
import withRedux from 'next-redux-wrapper';
import {Component} from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import nookies from 'nookies';
import React from "react";
import {setAxiosDefault} from "../utils/auth";

/*
FontAwesome
 */
import {config} from '@fortawesome/fontawesome-svg-core';
import '@fortawesome/fontawesome-svg-core/styles.css';
config.autoAddCss = false;

import '../scss/styles.scss';
import 'cropperjs/dist/cropper.min.css';

class MyApp extends App {
  static async getInitialProps({Component, ctx}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    const {_token} = nookies.get(ctx);
    await setAxiosDefault(_token);
    return {pageProps: pageProps, _token};
  }

  render() {
    setAxiosDefault(this.props._token);
    const {Component, pageProps, store} = this.props;
    return (
      <Provider store={store}>
        <PersistGate persistor={store.__PERSISTOR} loading={null}>
          <Component {...pageProps} />
        </PersistGate>
      </Provider>
    );
  }
}

export default withRedux(reduxStore)(MyApp);