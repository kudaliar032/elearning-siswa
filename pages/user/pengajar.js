import Head from "next/head";

const page = () => {
  if (typeof window !== 'undefined') {
    window.location.replace(process.env.NEXT_PUBLIC_PENGAJAR);
  }

  return (
    <>
      <Head>
        <title>Mengalihkan...</title>
      </Head>
      <div className="d-flex justify-content-center align-items-center h-100">
        <div className="spinner-grow text-primary" role="status" style={{width: "5rem", height: "5rem"}}>
          <span className="sr-only">Mengalihkan...</span>
        </div>
      </div>
    </>
  )
};

export default page;