import {Component} from 'react';
import Head from "next/head";
import Link from "next/link";
import axios from 'axios';
import {login, serverSideRootRedirect, userLoggedInCheck} from "../../../utils/auth";
import {connect} from 'react-redux';
import {getProfile} from "../../../redux/actions/userProfile";
import Swal from 'sweetalert2';

class siswa extends Component {
  static getInitialProps = ctx => {
    try {
      userLoggedInCheck(ctx);
      return {}
    } catch (e) {
      // console.log(e)
      serverSideRootRedirect(ctx);
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      username: '',
      email: '',
      password: '',
      passwordValidate: '',
      thisAccept: false
    };

    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(e) {
    const {name, value} = e.target;
    this.setState({[name]: value})
  }

  handleAccept() {
    this.setState({thisAccept: !this.state.thisAccept})
  }

  handleRegister(e) {
    e.preventDefault();
    const {password, passwordValidate} = this.state;
    if (password === passwordValidate) {
      Swal.fire({
        icon: "question",
        title: "Anda Yakin",
        html: "Tekan <b>Ya</b> untuk mengirimkan pendaftaran anda",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Batal",
        heightAuto: false
      }).then(({value}) => {
        if (value) {
          const {firstName, lastName, username, email, password} = this.state;
          const name = `${firstName} ${lastName}`;
          axios.post('/register', {name, username, email, password}).then(res => {
            this.props.getProfile(res.data.token); // get profile detail
            login(res.data.token); // login function
          }).catch(err => {
            Swal.fire({
              icon: "error",
              title: "Gagal",
              text: "Pendaftaran akun siswa gagal, coba ulangi lagi dengan data yang lain",
              heightAuto: false
            });
          });
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Pendaftaran Gagal",
        html: "Pastikan nilai password dan konfirmasi password telah sama",
        confirmButtonText: "OK",
        heightAuto: false
      })
    }
  }

  render() {
    const {firstName, lastName, username, email, password, passwordValidate, thisAccept} = this.state;
    const formValid = firstName && lastName && username && email && password && thisAccept;
    return (
      <div className="h-100">
        <Head>
          <title>Register | Siswa</title>
        </Head>
        <div className="container-fluid h-100 poppins-font">
          <div className="row h-100">
            <div className="col-md-5 bg-blue">
              <div className="d-flex flex-column justify-content-center h-100">
                <h1 className="mx-5 text-white">Platform pembelajaran <br/>E-Learning gratis.</h1>
                <div className="row justify-content-center">
                  <div className="col-md-12">
                    <img src="/img/left-register.svg" alt="login left image" className="img-fluid"/>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-7">
              <div className="d-flex flex-column justify-content-center h-100 px-5 register-style">
                <div className="row justify-content-center mb-3">
                  <img src="/img/logo-text.svg" alt="text logo" className="col-md-3"/>
                </div>
                <p className="text-gray-blue text-center mb-4">Silahkan daftar sebagai siswa</p>
                <div className="row justify-content-center">
                  <div className="col-8">
                    <form className="mx-5 mb-2 mt-2" method="post" onSubmit={this.handleRegister.bind(this)}>
                      <div className="row">
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="firstName">Nama depan</label>
                            <input type="text" className="form-control" id="firstName"
                                   name="firstName" placeholder="John" value={firstName} onChange={this.handleInput}
                                   required/>
                          </div>
                        </div>
                        <div className="col">
                          <div className="form-group">
                            <label htmlFor="lastName">Nama belakang</label>
                            <input type="text" className="form-control" id="lastName"
                                   name="lastName" placeholder="Doe" value={lastName} onChange={this.handleInput}
                                   required/>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" name="username"
                               placeholder="username" value={username} onChange={this.handleInput} required/>
                      </div>
                      <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" id="email" name="email"
                               placeholder="your@mail.com" value={email} onChange={this.handleInput} required/>
                      </div>
                      <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password"
                               name="password" placeholder="password" value={password} onChange={this.handleInput}
                               required/>
                      </div>
                      <div className="form-group">
                        <label htmlFor="passwordValidate">Konfirmasi Password</label>
                        <input type="password" className="form-control" id="passwordValidate"
                               name="passwordValidate" placeholder="konfirmasi password" value={passwordValidate} onChange={this.handleInput}
                               required/>
                      </div>
                      <div className="row">
                        <div className="col-1">
                          <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="accept" checked={thisAccept}
                                   onChange={this.handleAccept.bind(this)}/>
                            <label className="custom-control-label" htmlFor="accept">&nbsp;</label>
                          </div>
                        </div>
                        <div className="col-11">
                          <p className="ketentuan">Membuat akun berarti Anda setuju dengan <a href="#">Ketentuan
                            Layanan</a>, <a href="#">Kebijakan Privasi</a>, dan <a href="#">Pengaturan Pemberitahuan
                            default kami</a>.</p>
                        </div>
                      </div>
                      <button disabled={!formValid} type="submit" className="btn btn-block mt-2">BUAT AKUN</button>
                    </form>
                  </div>
                </div>
                <p className="text-blue-white text-center">
                  Sudah memiliki akun? Silahkan <Link href="/user/[page]" as="/user/login"><a><b>Masuk</b></a></Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getProfile: _token => dispatch(getProfile(_token))
});

export default connect(null, mapDispatchToProps)(siswa);