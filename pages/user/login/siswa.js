import {Component} from 'react';
import Link from 'next/link';
import Head from "next/head";
import axios from "axios";
import {getProfile} from "../../../redux/actions/userProfile";
import {connect} from 'react-redux';
import {login, serverSideRootRedirect, userLoggedInCheck} from "../../../utils/auth";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye, faEyeSlash, faLock, faSpinner, faUserAlt} from "@fortawesome/free-solid-svg-icons";

class page extends Component {
  static getInitialProps = ctx => {
    try {
      userLoggedInCheck(ctx);
      return {}
    } catch (e) {
      // console.log(e)
      serverSideRootRedirect(ctx);
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      passwordVisible: false,
      username: '',
      password: '',
      loginInvalid: false,
      isLoading: false
    }
  }

  handleInputChange(e) {
    const {name, value} = e.target;
    this.setState({[name]: value})
  }

  loadingStart() {this.setState({isLoading: true})}
  loadingStop() {this.setState({isLoading: false})}

  submitLogin(e) {
    e.preventDefault();
    const {username, password} = this.state;
    axios.post('/login', {username, password}).then(async res => {
      await this.loadingStart();
      const {token} = res.data;
      this.props.getProfile(token); // get profile detail
      login(token); // login function
    }).catch(err => {
      this.setState({loginInvalid: true});
      this.loadingStop();
    });
  };

  render() {
    const {username, password, passwordVisible, loginInvalid, isLoading} = this.state;
    return (
      <div className="h-100">
        <Head>
          <title>Login | Siswa</title>
        </Head>
        <div className="container-fluid h-100 poppins-font">
          <div className="row h-100">
            <div className="col-md-7">
              <div className="d-flex flex-column justify-content-center h-100 mx-4">
                <div className="row justify-content-center">
                  <img src="/img/logo-text.svg" alt="text logo" className="col-md-3"/>
                </div>
                <div className="row justify-content-center">
                  <div className="col-md-8">
                    <img src="/img/left-login.svg" alt="login left image" className="img-fluid"/>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-5 bg-blue">
              <div className="d-flex flex-column justify-content-center h-100 px-5 login-style">
                <h1>Selamat Datang!</h1>
                <h5 className="mb-4">Silahkan masuk menggunakan akun siswa</h5>
                <form className="mt-5 mb-4 mx-5" method="post" onSubmit={this.submitLogin.bind(this)}>
                  <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <div className={`input-group ${loginInvalid ? 'is-invalid' : null}`}>
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <FontAwesomeIcon icon={faUserAlt}/>
                        </span>
                      </div>
                      <input
                        value={username}
                        onChange={this.handleInputChange.bind(this)}
                        type="text"
                        className="form-control py-4"
                        id="username"
                        name="username"
                        placeholder="Masukan username"
                      />
                    </div>
                    <div className="invalid-feedback">Username salah</div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <div className={`input-group ${loginInvalid ? 'is-invalid' : null}`}>
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <FontAwesomeIcon icon={faLock}/>
                        </span>
                      </div>
                      <input value={password} onChange={this.handleInputChange.bind(this)}
                             type={!this.state.passwordVisible ? 'password' : 'text'}
                             className="form-control py-4" id="password" name="password"
                             placeholder="Masukan password"/>
                      <div className="input-group-append">
                        <button className="btn" type="button" onClick={() => {
                          this.setState({passwordVisible: !this.state.passwordVisible})
                        }}>
                          <FontAwesomeIcon icon={this.state.passwordVisible ? faEyeSlash : faEye}/>
                        </button>
                      </div>
                    </div>
                    <div className="invalid-feedback">Password salah</div>
                  </div>
                  <button disabled={!(username && password)} type="submit" className="btn btn-block py-2 mt-5">
                    {isLoading ? <FontAwesomeIcon icon={faSpinner} className="mr-2" pulse/> : null}MASUK
                  </button>
                </form>
                <p className="text-gray-blue text-center">
                  Belum memiliki akun? Silahkan <Link href="/user/[page]" as="/user/register"><a>Daftar Akun</a></Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  getProfile: token => dispatch(getProfile(token))
});

export default connect(null, mapDispatchToProps)(page);