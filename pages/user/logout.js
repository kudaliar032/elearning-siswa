import Router from 'next/router';
import {Component} from 'react';
import JsCookie from 'js-cookie';
import {connect} from 'react-redux';
import {resetProfile} from "../../redux/actions/userProfile";
import Head from 'next/head';

class logout extends Component {
  async componentDidMount() {
    await JsCookie.remove('_token');
    await this.props.resetProfile();
    await Router.push('/user/login');
  }

  render() {
    return (
      <>
        <Head>
          <title>Loading...</title>
        </Head>
        <div className="d-flex justify-content-center align-items-center h-100">
          <div className="spinner-grow text-primary" role="status" style={{width: "5rem", height: "5rem"}}>
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      </>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  resetProfile: () => dispatch(resetProfile())
});

export default connect(null, mapDispatchToProps)(logout);