import {Component} from 'react';
import Head from 'next/head';
import Link from 'next/link';
import LeftSide from "../../src/components/loginRegister/LeftSide";
import RightButtonBar from "../../src/components/loginRegister/RightButtonBar";
import {serverSideRootRedirect, userLoggedInCheck} from "../../utils/auth";

class page extends Component {
  static getInitialProps = ctx => {
    try {
      userLoggedInCheck(ctx);
      const {page} = ctx.query;

      const readyPage = ['login', 'register'];
      if (!readyPage.includes(page)) {
        throw 'Page not found';
      }

      return {
        page,
        login: {
          title: 'Login',
          bar: [
            {
              title: 'Masuk Sebagai Siswa',
              subtitle: 'Silahkan login menggunakan akun siswa',
              href: '/user/login/siswa',
            },
            {
              title: 'Masuk Sebagai Pengajar',
              subtitle: 'Silahkan login menggunakan akun pengajar',
              href: '/user/pengajar'
            }
          ]
        },
        register: {
          title: 'Register',
          bar: [
            {
              title: 'Daftar Sebagai Siswa',
              subtitle: 'Silahkan daftar menggunakan akun siswa',
              href: '/user/register/siswa',
            },
            // {
            //   title: 'Daftar Sebagai Pengajar',
            //   subtitle: 'Silahkan daftar menggunakan akun pengajar',
            //   href: '/user/pengajar',
            // }
          ]
        }
      };
    } catch (e) {
      // console.log(e)
      serverSideRootRedirect(ctx);
    }
  };

  render() {
    const {login, register, page} = this.props;
    return (
      <div className="h-100">
        <Head>
          <title>{page === 'login' ? login.title : register.title}</title>
        </Head>
        <div className="container-fluid h-100 poppins-font">
          <div className="row h-100">
            <div className="col-md-5 bg-blue text-white h-100">
              <LeftSide/>
            </div>
            <div className="col-md-7">
              <div className="d-flex flex-column justify-content-center h-100 px-5">
                <div className="row justify-content-center">
                  <img src="/img/logo-text.svg" alt="text logo" className="col-md-3"/>
                </div>
                {
                  page === 'login' ? (
                    login.bar.map((value, key) => (
                      <RightButtonBar
                        key={key}
                        title={value.title}
                        subtitle={value.subtitle}
                        href={value.href}
                      />
                    ))
                  ) : (
                    register.bar.map((value, key) => (
                      <RightButtonBar
                        key={key}
                        title={value.title}
                        subtitle={value.subtitle}
                        href={value.href}
                      />
                    ))
                  )
                }

                <div className="text-center mt-4">
                  {
                    page === 'login' ? (
                      <p className="text-blue-white">
                        Belum memiliki akun? Silahkan <Link href="/user/[page]" as="/user/register"><a>Daftar Akun</a></Link>
                      </p>
                    ) : (
                      <p className="text-blue-white">
                        Sudah memiliki akun? Silahkan <Link href="/user/[page]" as="/user/login"><a>Masuk</a></Link>
                      </p>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default page;