import {Component} from 'react';
import dynamic from "next/dynamic";
import CardAllCourse from "../../src/components/PageTemplate/Beranda/CardAllCourse";
import {handleAuthenticatedError, initialSetAxiosToken} from "../../utils/auth";
import axios from 'axios';
import Router from "next/router";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes, faTimesCircle} from "@fortawesome/free-solid-svg-icons";

const PageTemplate = dynamic(() => import('../../src/components/PageTemplate'), {ssr: false});

class page extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const {study, name} = ctx.query;
      let allCourse;
      if (study && name) {allCourse = await axios.get(`/course/list/not?study=${study}&name=${name}`)}
      else if (study) {allCourse = await axios.get(`/course/list/not?study=${study}`)}
      else if (name) {allCourse = await axios.get(`/course/list/not?name=${name}`)}
      else {allCourse = await axios.get('/course/list/not')}

      return {
        allCourse: allCourse.data
      }
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {allCourse} = this.props;
    const {study, name} = Router.query;
    this.state = {
      allCourse: allCourse ? allCourse : [],
      study, name
    };

    this.updateCourseList = this.updateCourseList.bind(this);
  }

  updateCourseList(study, name) {
    let axiosUrl, routeUrl;
    if (study && name) {
      axiosUrl = `/course/list/not?study=${study}&name=${name}`;
      routeUrl = `/course/all?study=${study}&name=${name}`;
    }
    else if (study) {
      axiosUrl = `/course/list/not?study=${study}`;
      routeUrl = `/course/all?study=${study}`;
    }
    else if (name) {
      axiosUrl = `/course/list/not?name=${name}`;
      routeUrl = `/course/all?name=${name}`;
    }
    else {
      axiosUrl = `/course/list/not`;
      routeUrl = `/course/all`;
    }

    axios.get(axiosUrl).then(async res => {
      this.setState({allCourse: res.data, study, name});
      Router.push(routeUrl);
    }).catch(err => {handleAuthenticatedError(err, null);});
  };

  updateAllData() {
    const {study, name} = this.state;
    this.updateCourseList(study, name);
  }

  searchFunction(searchValue) {
    const {study} = this.state;
    this.updateCourseList(study, searchValue);
  }

  render() {
    const {allCourse, study, name} = this.state;
    const studyName = {matematika: 'Matematika', ipa: 'IPA', indonesia: 'Bahasa Indonesia', inggris: 'Bahasa Inggris'};
    return (
      <PageTemplate title="Materi Lain" searchBar searchFunction={this.searchFunction.bind(this)}>
        <div className="row mb-4">
          <section className="col">
            <div className="d-flex flex-row course-header align-items-center">
              <div className="d-flex mr-auto align-items-center">
                <h5>Materi lain dari <span>{study ? '"'+studyName[study]+'"' : '"Semua Materi"'}</span></h5>
                {name ? (
                  <h5>, dengan pencarian <span>"{name}"</span></h5>
                ) : null}
              </div>
              <div>
                <div className="btn-group filter-by">
                  <button type="button" className="btn border-0">
                    <span>Kategori:</span> {study ? studyName[study] : 'Semua Materi'}
                  </button>
                  {
                    study ?
                      <button onClick={() => {this.updateCourseList(null, name)}} type="button" className="btn border-0 reset">
                        <FontAwesomeIcon icon={faTimesCircle}/>
                      </button> : null
                  }
                  <button type="button" className="btn dropdown-toggle border-0 icon" data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false" data-offset="0, 10"></button>
                  <div className="dropdown-menu dropdown-menu-right">
                    <Link href="/course/all?study=matematika">
                      <a onClick={() => {
                        this.updateCourseList('matematika', name)
                      }} className={`dropdown-item ${study === 'matematika' ? 'active' : null}`}>Matematika</a>
                    </Link>
                    <hr/>
                    <Link href="/course/all?study=ipa">
                      <a onClick={() => {
                        this.updateCourseList('ipa', name)
                      }} className={`dropdown-item ${study === 'ipa' ? 'active' : null}`}>IPA</a>
                    </Link>
                    <hr/>
                    <Link href="/course/all?study=indonesia">
                      <a onClick={() => {
                        this.updateCourseList('indonesia', name)
                      }} className={`dropdown-item ${study === 'indonesia' ? 'active' : null}`}>Bahasa Indonesia</a>
                    </Link>
                    <hr/>
                    <Link href="/course/all?study=inggris">
                      <a onClick={() => {
                        this.updateCourseList('inggris', name)
                      }} className={`dropdown-item ${study === 'inggris' ? 'active' : null}`}>Bahasa Inggris</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="row">
          <section className="col">
            <div className={allCourse.length > 0 ? 'row row-cols-2 row-cols-md-3' : 'row row-col-1 p-2'}>
              {
                allCourse.length > 0 ? allCourse.map((val, key) => (
                  <CardAllCourse
                    key={key}
                    id={val.id}
                    courseName={val.name}
                    courseStudy={val.study}
                    header={val.header}
                    totalModules={val.total_modules}
                    updateAllData={this.updateAllData.bind(this)}
                  />
                )) : (
                  <h3 className="text-gray-blue">Tidak ditemukan materi</h3>
                )
              }
            </div>
          </section>
        </div>
      </PageTemplate>
    );
  }
}

export default page;