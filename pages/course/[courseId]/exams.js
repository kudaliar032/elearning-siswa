import {Component} from 'react';
import {handleAuthenticatedError, initialSetAxiosToken} from "../../../utils/auth";
import Router from "next/router";
import {connect} from 'react-redux';
import {updateAnswer} from "../../../redux/actions/multipleChoices";
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from "sweetalert2-react-content";
import {resetMultipleChoices} from "../../../redux/actions/multipleChoices";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";
import Head from "next/head";
import nookies from 'nookies';
import Timer from "../../../src/components/PageTemplate/Exam/Timer";
import JsCookie from 'js-cookie';

const AnswerSwal = withReactContent(Swal);

const AnswerAlert = ({totalTrue, totalQuestions, totalAnswers, courseId, courseName}) => {
  const score = 100 * (totalTrue / totalQuestions);
  return (
    <div className="row d-flex flex-column p-5 exams-alert poppins-font">
      <img src={`/img/exams/${score >= 75 ? 'success' : 'fail'}.svg`} alt="Exams Success" className="img-fluid col-2 offset-5"/>
      <h1 className="py-2">{score >= 75 ? 'Selamat' : 'Aduh'}!</h1>
      <h3 className="py-2">Nilai: {score.toFixed(1)} / 100</h3>
      <p className="py-2">Kamu telah menyelesikan <span>{totalAnswers}</span> dari <span>{totalQuestions} Soal</span>,
        total <span>jawaban benar {totalTrue}</span> dan total <span>jawaban salah {totalQuestions - totalTrue}</span>.
        {score >= 75 ? ` Yuk terus tingkatkan prestasimu!` : ` Nilaimu masih kurang, silahkan ulangi lagi materi yang kamu pelajari, untuk lebih memeperdalam ${courseName}.`}</p>
      <button className="btn bg-pink border-0 col-4 offset-4" onClick={() => {
        AnswerSwal.clickConfirm();
        Router.push('/course/[courseId]', `/course/${courseId}`);
      }}>Kembali</button>
    </div>
  )
};

class exams extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const {courseId, no} = ctx.query;
      const {start_exam} = nookies.get(ctx);

      const questions = await axios.get(`/exam/questions/${courseId}`);
      const {data} = await axios.get(`/course/detail/${courseId}`);

      if (no === undefined || ((no > questions.data.length) && questions.data.length > 0) || !parseInt(no)) {
        ctx.res.writeHead(302, {Location: `/course/${courseId}/exams?no=1`});
        ctx.res.end();
        return {};
      }

      return {
        courseName: data.detail.name,
        startExam: parseInt(start_exam),
      };
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {courseId, no} = Router.query;
    const {startExam} = this.props;
    this.state = {
      courseId: parseInt(courseId),
      no: parseInt(no),
      startExam
    };

    this.changeQuestionsByNumber = this.changeQuestionsByNumber.bind(this);
  }

  componentDidMount() {
    $('body').addClass('my-bg');
    AnswerSwal.clickCancel();

    if (this.props.multipleChoices.length < 1) {
      this.props.resetMultipleChoices();
      Router.push(`/course/[courseId]`,`/course/${this.state.courseId}`)
    }
  }

  changeQuestionsByNumber(num) {
    this.setState({no: num});
    Router.push(
      `/course/[courseId]/exams?no=${num}`,
      `/course/${this.state.courseId}/exams?no=${num}`,
      {shallow: true}
    );
  }

  nextQuestion() {
    const {no} = this.state;
    const nextNumber = no + 1;
    this.changeQuestionsByNumber(nextNumber);
  };

  backQuestion() {
    const {no} = this.state;
    const backNumber = no - 1;
    this.changeQuestionsByNumber(backNumber);
  };

  sendMyAnswer() {
    let totalAnswers = 0;
    const answers = this.props.multipleChoices.map(({id, answer}) => {
      if (answer !== null) {
        totalAnswers++;
      }
      return ({id, answer});
    });
    const data = {
      answers, course_id: this.state.courseId
    };
    axios.post(`/exam/answers/check`, data).then(async res => {
      const {total_true, total_questions} = res.data.data;
      await this.props.resetMultipleChoices();
      JsCookie.remove('start_exam');
      AnswerSwal.fire({
        html: <AnswerAlert
          totalTrue={total_true}
          totalQuestions={total_questions}
          totalAnswers={totalAnswers}
          resetRedux={this.props.resetMultipleChoices}
          courseId={this.state.courseId}
          courseName={this.props.courseName}
        />,
        allowOutsideClick: false,
        showConfirmButton: false,
        allowEscapeKey: false,
        width: '60%'
      });
    }).catch(err => {
      handleAuthenticatedError(err, null);
    });
  }

  timeoutFunc() {
    if (this.props.multipleChoices.length > 0) {
      Swal.fire({
        icon: 'error',
        title: 'Waktu habis',
        html: 'Waktu anda menjawab latihan ini telah habis, tekan <b>Ya</b> untuk menampilkan hasil latihan',
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonText: 'Ya'
      }).then(({value}) => {
        if (value) {
          this.sendMyAnswer();
        }
      });
    }
  }

  render() {
    const {no, startExam} = this.state;
    const {courseName, updateAnswer} = this.props;
    const questions = this.props.multipleChoices;
    const questionIdx = no - 1;

    return (
      <>
        <Head>
          <title>Latihan Soal | {courseName}</title>
        </Head>
        <div className="p-5 poppins-font">
          {questions.length > 0 ? <h3 className="p-0 mb-0">{courseName}</h3> : null}
          <div className="row mb-4">
            <section className="col-4 rounded-lg p-3 pr-4 left-exam">
              <div className="row bg-white rounded-lg shadow">
                {
                  questions.length > 0 ? (
                    <div className="col d-flex flex-column p-4">
                      <div>
                        <button
                          onClick={() => {
                            Swal.fire({
                              icon: 'info',
                              title: 'Selesaikan latihan',
                              html: 'Dengan menekan <b>Ya</b>, jawaban anda saat ini akan disimpan. Tekan <b>Batal</b> jika merasa belum yakin',
                              showCancelButton: true,
                              cancelButtonText: 'Batal',
                              confirmButtonText: 'Ya',
                            }).then(res => {
                              if (res.value) {
                                this.sendMyAnswer();
                              }
                            })
                          }}
                          className="btn btn-block bg-pink py-3 border-0 mb-3"
                        >Selesaikan Latihan</button>
                      </div>
                      <Timer startExam={startExam} timeoutFunc={this.timeoutFunc.bind(this)}/>
                      <div>
                        <hr/>
                      </div>
                      <h6>Daftar Soal:</h6>
                      <div className="row">
                        {
                          questions.map((val, key) => (
                            <div key={key} className={`col p-2 task-number`} style={{minWidth: '20%', maxWidth: '20%'}}>
                              <button
                                onClick={() => {
                                  this.changeQuestionsByNumber(key + 1)
                                }}
                                className={`btn btn-block btn-lg py-3 border-0 ${key === questionIdx ? 'now' : questions[key].answer ? 'active' : null}`}
                              >{key + 1}</button>
                            </div>
                          ))
                        }
                      </div>
                    </div>
                  ) : null
                }
              </div>
            </section>
            {
              questions[questionIdx] !== undefined ? (
                <section className="col-8 rounded-lg p-3 pl-4 right-exam">
                  <div className="row bg-white rounded-lg p-4 shadow">
                    <div className="col d-flex flex-column">
                      <div className="d-flex">
                        <h5 className="mr-auto my-auto">Pertanyaan {no} dari {questions.length}</h5>
                        {
                          no === 1 ? null : (
                            <button
                              onClick={() => {this.backQuestion()}}
                              className="my-auto btn bg-gray-light mr-2">
                              <FontAwesomeIcon icon={faChevronLeft}/>
                            </button>
                          )
                        }
                        {
                          no === questions.length ? null : (
                            <button
                              onClick={() => {this.nextQuestion()}}
                              className="my-auto btn bg-gray-light">
                              <FontAwesomeIcon icon={faChevronRight}/>
                            </button>
                          )
                        }
                      </div>
                      <div className="mt-3">
                        <div dangerouslySetInnerHTML={{__html: questions[questionIdx].question}}></div>
                      </div>
                      <div className="mt-3 p-0">
                        <ul className="list-group border-0 p-0">
                          {
                            ['a', 'b', 'c', 'd', 'e'].map(val => (
                              <li key={val} className="list-group-item border-0 px-0 py-2">
                                <button
                                  onClick={() => {updateAnswer(questions[questionIdx].id, val)}}
                                  className={`btn btn-primary answer-button mr-3 ${questions[questionIdx].answer === val ? 'bg-pink' : null}`}>
                                  {val.toUpperCase()}
                                </button>
                                {questions[questionIdx][val]}
                              </li>
                            ))
                          }
                        </ul>
                      </div>
                    </div>
                  </div>
                </section>
              ) : null
            }
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  multipleChoices: state.multipleChoices
});

const mapDispatchToProps = dispatch => ({
  updateAnswer: (id, answer) => dispatch(updateAnswer(id, answer)),
  resetMultipleChoices: () => dispatch(resetMultipleChoices())
});

export default connect(mapStateToProps, mapDispatchToProps)(exams);