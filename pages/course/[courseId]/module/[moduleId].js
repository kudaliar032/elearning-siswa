import {Component} from 'react';
import {handleAuthenticatedError, initialSetAxiosToken} from "../../../../utils/auth";
import axios from "axios";
import dynamic from "next/dynamic";
import Link from "next/link";
import Router from "next/router";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faCheckCircle,
  faAngleLeft,
  faCircle,
  faPlayCircle, faArrowRight, faArrowLeft,
} from "@fortawesome/free-solid-svg-icons";

const PageTemplate = dynamic(() => import('../../../../src/components/PageTemplate'), {ssr: false});

class moduleId extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const {courseId, moduleId} = ctx.query;
      const moduleList = await axios.get(`/module/list/${courseId}`);
      const detailModule = await axios.get(`/module/detail/${courseId}/${moduleId}`);

      return {
        moduleList: moduleList.data.modules,
        detailModule: detailModule.data,
        courseId: parseInt(courseId),
        moduleId: parseInt(moduleId)
      }
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);
    const {detailModule, moduleList, moduleId, courseId} = this.props;
    this.state = {detailModule, moduleList, courseId, moduleId};

    this.changeModule = this.changeModule.bind(this);
    this.backModule = this.backModule.bind(this);
    this.nextModule = this.nextModule.bind(this);
  }

  async changeModule(moduleId) {
    try {
      const moduleList = await axios.get(`/module/list/${this.state.courseId}`);
      const detailModule = await axios.get(`/module/detail/${this.state.courseId}/${moduleId}`);
      this.setState({
        moduleList: moduleList.data.modules,
        detailModule: detailModule.data,
        moduleId: parseInt(moduleId)
      });
      Router.push(
        '/course/[courseId]/module/[moduleId]',
        `/course/${this.state.courseId}/module/${moduleId}`,
        {shallow: true});
    } catch (e) {
      handleAuthenticatedError(e, null);
    }
  }

  backModule(moduleIndex) {
    const backId = moduleIndex-1;
    const moduleId = this.state.moduleList[backId].id;
    this.changeModule(moduleId);
  }

  nextModule(moduleIndex) {
    const nextId = moduleIndex+1;
    const {courseId, moduleList} = this.state;
    const moduleId = moduleList[nextId].id;
    axios.post(`/user/open/module/${courseId}/${moduleId}`).then(res => {
      this.changeModule(moduleId);
    }).catch(err => {
      handleAuthenticatedError(err, null);
    });
  }

  render() {
    const {lastOpen} = this.state;
    const modules = this.state.moduleList;
    const {title, module_content} = this.state.detailModule;
    const {courseId, moduleId} = this.state;
    const moduleIndex = modules.findIndex(x => x.id == moduleId);
    return (
      <PageTemplate title="Module">
        <div className="row mb-4">
          <section className="col-8 p-3 pr-4">
            <div className="row bg-white rounded-lg">
              <div className="col p-4">
                <div className="d-flex">
                  <h3 className="back-button">
                    <Link href="/course/[courseId]" as={`/course/${courseId}`}>
                      <a><FontAwesomeIcon icon={faAngleLeft} className="mr-3"/></a>
                    </Link>
                  </h3>
                  <h3>{title}</h3>
                </div>
                <div className="pt-3" dangerouslySetInnerHTML={{__html: module_content}}></div>
                <div className="d-flex pt-4">
                  {
                    moduleIndex === 0 ? null : (
                      <button
                        onClick={() => {this.backModule(moduleIndex)}}
                        className="btn bg-pink border-0">
                        <FontAwesomeIcon icon={faArrowLeft} className="mr-2"/> Sebelumnya
                      </button>
                    )
                  }
                  {
                    moduleIndex === (modules.length - 1) ? null : (
                      <button
                        onClick={() => {this.nextModule(moduleIndex)}}
                        className="btn bg-pink border-0 ml-auto">
                        Berikutnya <FontAwesomeIcon icon={faArrowRight} className="ml-2"/></button>
                    )
                  }
                </div>
              </div>
            </div>
          </section>
          <section className="col-4 p-3 pl-4">
            <div className="row bg-white rounded-lg">
              <div className="col py-4 px-3">
                {
                  modules.map((val, key) => (
                    <button disabled={!(val.done || (!lastOpen && key === 0))}
                            key={key}
                            onClick={() => {this.changeModule(val.id)}}
                            className={`list-group-item list-group-item-action ${val.done || (!lastOpen && key === 0) ? null : 'module-disable'} d-flex border-0 rounded-lg module-list mb-2`}>
                      <div className="my-auto row pr-3">
                        <span className="fa-layers fa-fw fa-2x">
                          <FontAwesomeIcon icon={faCircle} className="text-op-blue"/>
                          <span className="fa-layers-text fa-inverse" style={{fontWeight: 600}}
                                data-fa-transform="shrink-8">{key + 1}</span>
                        </span>
                      </div>
                      <h6 className="my-auto pr-2 text-dark">{val.title}</h6>
                      <div className="my-auto row pr-2">
                        {
                          val.done || (!lastOpen && key === 0) ? (
                            parseInt(val.id) !== parseInt(moduleId) ?
                              <FontAwesomeIcon icon={faCheckCircle} size="lg" className="text-pink"/> :
                              <FontAwesomeIcon icon={faPlayCircle} size="lg"/>
                          ) : null
                        }
                      </div>
                    </button>
                  ))
                }
              </div>
            </div>
          </section>
        </div>
      </PageTemplate>
    )
  }
}

export default moduleId;