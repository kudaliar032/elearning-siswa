import {Component} from 'react';
import dynamic from 'next/dynamic';
import Router from "next/router";
import {handleAuthenticatedError, initialSetAxiosToken} from "../../utils/auth";
import axios from "axios";
import Link from "next/link";
import PinkProgressBar from "../../src/components/PageTemplate/PinkProgressBar";
import {connect} from 'react-redux';
import {fetchMultipleChoices} from "../../redux/actions/multipleChoices";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faAngleLeft,
  faBook,
  faCheckCircle,
  faCircle,
  faPencilRuler,
  faPlayCircle
} from "@fortawesome/free-solid-svg-icons";
import JsCookie from 'js-cookie';
import moment from "moment";

const PageTemplate = dynamic(() => import('../../src/components/PageTemplate'), {ssr: false});

class courseId extends Component {
  static getInitialProps = async ctx => {
    try {
      initialSetAxiosToken(ctx);
      const res = await axios.get(`/course/detail/${ctx.query.courseId}`);

      return {
        detail: res.data.detail,
        modules: res.data.modules
      }
    } catch (e) {
      handleAuthenticatedError(e, ctx);
    }
  };

  constructor(props) {
    super(props);

    this.openModule = this.openModule.bind(this);
  }

  openModule(moduleId) {
    const courseId = this.props.detail.id;
    axios.post(`/user/open/module/${courseId}/${moduleId}`).then(res => {
      Router.push('/course/[courseId]/module/[moduleId]', `/course/${courseId}/module/${moduleId}`)
    }).catch(err => {
      handleAuthenticatedError(err, null);
    });
  }

  render() {
    const {modules} = this.props;
    const {id, study, name, description, modules_total, modules_done, exams_total, last_open} = this.props.detail;
    return (
      <PageTemplate title={name}>
        <div className="row mb-4">
          <section className="col bg-white rounded-lg p-3">
            <div className="d-flex course-detail-header pt-3">
              <div className="back-button px-2 pr-4">
                <Link href="/course">
                  <a><h3><FontAwesomeIcon icon={faAngleLeft} size="lg"/></h3></a>
                </Link>
              </div>
              <div className="w-100">
                <h3>{study}, Materi: {name}</h3>
                <div className="d-flex text-gray-blue">
                  <p className="mr-3"><FontAwesomeIcon icon={faPencilRuler}/> {exams_total} Soal</p>
                  <p><FontAwesomeIcon icon={faBook}/> {modules_total} Modul</p>
                </div>
                <div>
                  <div className="d-flex"><p className="mr-auto text-gray-blue">Proses Belajar</p>
                    <p>{modules_done} dari {modules_total} Modul</p></div>
                  <PinkProgressBar valNow={modules_done} maxVal={modules_total}/>
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col">
                <ul className="nav nav-tabs px-5" id="myTab" role="tablist">
                  <li className="nav-item">
                    <a className="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab"
                       aria-controls="description" aria-selected="true">Deskripsi</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" id="modules-tab" data-toggle="tab" href="#modules" role="tab"
                       aria-controls="modules" aria-selected="false">Modul</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" id="exams-tab" data-toggle="tab" href="#exams" role="tab"
                       aria-controls="exams" aria-selected="false">Latihan Soal</a>
                  </li>
                </ul>
                <div className="tab-content px-5 py-4" id="myTabContent">
                  <div className="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab" dangerouslySetInnerHTML={{__html: description}}></div>
                  <div className="tab-pane fade" id="modules" role="tabpanel" aria-labelledby="modules-tab">
                    <div className="list-group">
                      {
                        modules.map((val, key) => (
                          <button disabled={!(val.done || last_open == null && key === 0)} key={key} onClick={() => {this.openModule(val.id)}}
                               className={`list-group-item ${!val.done ? last_open == null && key === 0 ? 'list-group-item-action' : 'module-disable' : 'list-group-item-action'} d-flex border-0 rounded-lg module-list mb-2`}>
                            <div className="my-auto row pr-3">
                                  <span className="fa-layers fa-fw fa-2x">
                                    <FontAwesomeIcon icon={faCircle} className="text-op-blue"/>
                                    <span className="fa-layers-text fa-inverse" style={{fontWeight: 500}}>{key + 1}</span>
                                  </span>
                            </div>
                            <h6 className="my-auto mr-auto text-dark">{val.title}</h6>
                            <div className="my-auto">
                              {
                                val.done > 0 || (last_open == null && key === 0) ? (
                                  <FontAwesomeIcon icon={faCheckCircle} className="text-pink" size="lg"/>
                                ) : (
                                  <FontAwesomeIcon icon={faPlayCircle} size="lg"/>
                                )
                              }
                            </div>
                          </button>
                        ))
                      }
                    </div>
                  </div>
                  <div className="tab-pane fade" id="exams" role="tabpanel" aria-labelledby="exams-tab">
                    {
                      modules_done >= modules_total ? (
                        <div className="text-center m-5 text-gray-blue">
                          <h5>Selamat! anda telah menyelesaikan modul Aljabar untuk lebih memahami materi yang telah
                            dipelajari silahkan klik button dibawah ini, untuk memulai latihan</h5>
                          <button
                            onClick={() => {
                              this.props.fetchMultipleChoices(id);
                              JsCookie.set('start_exam', moment().unix())
                            }}
                            className="btn bg-pink text-white border-0 py-2 px-5 mt-4">Mulai Latihan</button>
                        </div>
                      ) : (
                        <div className="text-center m-5 text-gray-blue">
                          <h5>Anda perlu menyelesaikan daftar modul yang terdapat pada <a onClick={e => {
                            e.preventDefault();
                            $('#myTab a[href="#modules"]').tab('show');
                          }} className="text-blue" href="">Menu Modul</a> supaya bisa mengakses menu Latihan Soal</h5>
                        </div>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </PageTemplate>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchMultipleChoices: courseId => dispatch(fetchMultipleChoices(courseId))
});

export default connect(null, mapDispatchToProps)(courseId);