module.exports = {
  apps: [{
    name: "elearning-siswa",
    script: "npm run start",
    env: {
      NODE_ENV: "development"
    },
    env_production: {
      NODE_ENV: "production"
    }
  }],
};
