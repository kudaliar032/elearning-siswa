import nookies, {destroyCookie} from 'nookies';
import JsCookie from 'js-cookie';
import Router from 'next/router';
import axios from "axios";

export const userLoggedInCheck = ctx => {
  const {_token} = nookies.get(ctx);
  if (_token) {
    throw 'You have logged in';
  }
};

export const serverSideRootRedirect = (ctx) => {
  ctx.res.writeHead(302, {Location: '/'});
  ctx.res.end();
}

export const initialSetAxiosToken = ctx => {
  if (ctx) {
    const {_token} = nookies.get(ctx);
    setAxiosDefault(_token);
  }
};

export const handleAuthenticatedError = (err, ctx) => {
  if (err.response === undefined) {
    console.log(err);
    return {};
  } else if (err.response.status === 401) {
    console.log(err.response.data);
    if (typeof document === 'undefined') {
      destroyCookie(ctx, '_token');
      ctx.res.writeHead(302, {Location: '/user/login'});
      ctx.res.end();
      return {};
    } else {
      alert('Login session invalid, silahkan login dengan session baru');
      JsCookie.remove('_token');
      Router.push('/user/login');
      return {};
    }
  } else {
    console.log(err);
    return {};
  }
};

export const setAxiosDefault = token => {
  axios.defaults.baseURL = process.env.NEXT_PUBLIC_API;
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const login = async _token => {
  try {
    await setAxiosDefault(_token); // set axios token default
    await JsCookie.remove('_token'); // remove _token in cookies
    await JsCookie.set('_token', _token, {expires: 1, path: '/'}); // set cookie _token
    Router.push('/'); // redirect to home
  } catch (e) {
    console.log(e);
  }
};